﻿using DocumentWatcher.IO;
using DocumentWatcher.Models;
using DocumentWatcher.Services.Interfaces;
using log4net;
using System.Diagnostics;
using System.IO;

namespace DocumentWatcher.Services
{
    /// <summary>
    /// IO services
    /// </summary>
    public sealed class DocumentService : IDocumentService
    {
        private static ILog Log => LogManager.GetLogger(typeof(DocumentService));

        public Document CreateDocument((string fileName, byte[] binary) file)
        {
            var fullFilePath = Path.Combine(Common.WorkingDirectory, file.fileName);
            Log.Debug($"Creating file on disk : '{fullFilePath}'");

            try
            {
                // JIT create directory, if not present
                Directory.CreateDirectory(Common.WorkingDirectory);

                var document = new Document
                {
                    FilePath = fullFilePath,
                    Binary = file.binary
                };

                File.WriteAllBytes(document.FilePath, document.Binary);

                return document;
            }
            catch (IOException ex)
            {
                Log.Error($"Failed to create document '{fullFilePath}'");
                Log.Debug($"Failed to create document '{fullFilePath}'", ex);
                return null;
            }
        }

        public void OpenDocument(Document document)
        {
            if (Path.GetDirectoryName(document.FilePath) != Common.WorkingDirectory)
            {
                Log.Error($"File is not in the working directory '{document.FilePath}' and cannot be opened");
                return;
            }
            StartProcessForDocument(document);
        }

        private static void StartProcessForDocument(Document document)
        {
            Log.Debug($"Opening client's preferred document processor for file '{document.FilePath}'");

            if (!File.Exists(document.FilePath))
            {
                Log.Error($"Cannot open document, file doesn't exists '{document.FilePath}");
                return;
            }

            // wait until the document is opened succesfully, before ending this method
            Process.Start(document.FilePath)?.WaitForInputIdle();
        }

        public Document GetDocument(string fileName)
        {
            var document = new Document { FilePath = Path.Combine(Common.WorkingDirectory, fileName) };
            document.Binary = File.ReadAllBytes(document.FilePath);

            return document;
        }

        public void ActivateDocument(string fileName)
        {
            var filePath = Path.Combine(Common.WorkingDirectory, fileName);
            if (File.Exists(filePath))
            {
                Log.Debug($"Open '{filePath}'");
                Process.Start(new ProcessStartInfo(filePath));
            }
        }

        public bool DeleteDocument(string filePath)
        {
            if (Path.GetDirectoryName(filePath) != Common.WorkingDirectory)
            {
                Log.Error($"File is not in the working directory '{filePath}' and cannot be deleted");
                return false;
            }
            
            try
            {
                Log.Debug($"File '{filePath}' will be deleted");
                File.Delete(filePath);
                Log.Debug($"File '{filePath}' is deleted");
                return true;
            }
            catch (IOException ex) {
                Log.Error($"Failed to delete file '{filePath}'");
                Log.Debug($"Failed to delete file '{filePath}'", ex);
                return false;
            }
        }
    }
}