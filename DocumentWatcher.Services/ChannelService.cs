﻿using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Models;
using DocumentWatcher.Services.Interfaces;
using log4net;
using Microsoft.Practices.ServiceLocation;
using System;
using System.ServiceModel;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;

namespace DocumentWatcher.Services
{
    /// <summary>
    /// Inter process communication
    /// </summary>
    public sealed class ChannelService : IChannelService, IDisposable
    {
        private static ILog Log => LogManager.GetLogger(typeof(ChannelService));
        private ServiceHost _host;

        public bool Started => _host != null && _host.State != CommunicationState.Closed;

        public void StartChannel()
        {
            if (!Started)
            {
                _host = new ServiceHost(typeof(DocumentHost), new Uri($"{Shared.Settings.Default.ChannelEndpointAddress}/{GetCurrentUserSid()}")) { OpenTimeout = new TimeSpan(0, 5, 0) };
                _host.AddServiceEndpoint(typeof(IDocumentHost), new NetNamedPipeBinding(), Shared.Settings.Default.ChannelEndpointOperation);

                try
                {
                    _host.Open();
                    foreach (var serviceEndpoint in _host.Description.Endpoints)
                    {
                        Log.Info($"Channel opened, available endpoint listening at {serviceEndpoint.ListenUri.AbsoluteUri}");
                    }
                }
                catch (Exception ex)
                {
                    Log.Fatal("DocumentHost channel couldn't be opened.", ex);
                }
            }
        }

        public static void PublishWork(Instruction instruction)
        {
            try
            {
                using (var channelFactory = new ChannelFactory<IDocumentHost>(new NetNamedPipeBinding(), new EndpointAddress($"{Shared.Settings.Default.ChannelEndpointAddress}/{GetCurrentUserSid()}{Shared.Settings.Default.ChannelEndpointOperation}")))
                {
                    var documentHostProxy = channelFactory.CreateChannel();

                    Log.Debug($"Adding instruction to channel at {channelFactory.Endpoint.ListenUri.AbsoluteUri}");
                    documentHostProxy.AddInstruction(instruction);
                }
            }
            catch (Exception ex)
            {
                Log.Fatal("DocumentHost instruction couldn't be processed.", ex);
            }
        }

        public void StopChannel()
        {
            if (Started)
            {
                try
                {
                    _host.Close();
                }
                catch (Exception ex)
                {
                    Log.Fatal("DocumentHost channel couldn't be closed.", ex);
                }
            }
        }

        /**
         * Due to a problem with retrieving the sid of the current user when Azure Active Directory is being used
         * we should have a fallback in determining the SID. For backwards compatibility we are using this fallback
         * instead of only using the WindowsIdentity.
         */
        private static SecurityIdentifier GetCurrentUserSid()
        {
            try
            {
                return UserPrincipal.Current.Sid;
            }
            catch (InvalidCastException ex)
            {
                Log.Debug("Could not determine Sid through current user principal. Using fallback.", ex);
                // Use fallback for situation where due to the usage of azure active directory an exception occurs in the retrieval of the current user
                return WindowsIdentity.GetCurrent().User;
            }
        }

        #region IDisposable

        public void Dispose()
        {
            StopChannel();
        }

        #endregion IDisposable
    }

    /// <summary>
    /// Proxy
    /// </summary>
    public class DocumentHost : IDocumentHost
    {
        private static ILog Log => LogManager.GetLogger(typeof(DocumentHost));
        private readonly IWorkService _workService;

        public DocumentHost()
        {
            _workService = ServiceLocator.Current.GetInstance<IWorkService>();
        }

        public void AddInstruction(Instruction instruction)
        {
            Log.Debug("Receiving instruction from channel");
            _workService.StartWork(instruction);
        }
    }
}