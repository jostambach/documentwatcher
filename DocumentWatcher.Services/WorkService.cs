﻿using DocumentWatcher.IO;
using DocumentWatcher.IO.API;
using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Models;
using DocumentWatcher.Services.Interfaces;
using DocumentWatcher.Shared.Enums;
using log4net;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentWatcher.Services
{
    /// <summary>
    /// Work item engine
    /// </summary>
    public sealed class WorkService : IWorkService
    {
        private static ILog Log => LogManager.GetLogger(typeof(WorkService));
        private readonly IDatabase _database;
        private readonly IDocumentService _documentService;
        private readonly IFileWatcherService _fileWatcherService;

        public ObservableCollection<Work> AllWork { get; }
        public event Action<string, MessageType> Alert;
        public event Action<Work> ConfirmWork;

        public WorkService()
        {
            _documentService = ServiceLocator.Current.GetInstance<IDocumentService>();
            _database = ServiceLocator.Current.GetInstance<IDatabase>();
            _fileWatcherService = ServiceLocator.Current.GetInstance<IFileWatcherService>();

            // init from data file
            AllWork = new ObservableCollection<Work>(_database.All);
            AllWork.CollectionChanged += WorkCollectionChanged;
            AllWork.ToList().ForEach(w => w.PropertyChanged += WorkItemChanged);
        }

        /// <summary>
        /// API lock zaaksysteem
        /// API download binary + fileName header
        /// DB add to database => collection + gui
        /// IO save to disk
        /// IO open document processor
        /// IO start file watcher
        /// </summary>
        public async Task StartWork(Instruction instruction)
        {
            if (IsWorkInProgress(instruction))
            {
                Alert(Shared.Resources.NotifyInfoWorkInProgress, MessageType.Info);
            }
            else
            {
                Log.Debug($"Start working on document '{instruction.FileId}'");

                var hasLock = await ZaaksysteemClient.HasLockAsync(instruction);
                if (hasLock is false && await ZaaksysteemClient.SetLockAsync(instruction))
                {
                    var file = await ZaaksysteemClient.DownloadAsync(instruction);
                    if (!file.Equals(default))
                    {
                        var document = _documentService.CreateDocument(file);
                        if(document == null)
                        {
                            return;
                        }
                        _documentService.OpenDocument(document);

                        var work = new Work
                        {
                            FileName = file.fileName,
                            Instruction = instruction,
                            IsInSync = true,
                            LastLock = DateTime.Now,
                            Location = instruction.BaseUri.AbsoluteUri,
                            State = State.Busy,
                            WorkStarted = DateTime.Now,
                            Hash = Common.CalculateHash(document.Binary),
                            FileLockReceived = false
                        };
                        work.PropertyChanged += WorkItemChanged;
                        AllWork.Add(work);

                        _fileWatcherService.StartWatcher();
                    }
                    else
                    {
                        Alert(Shared.Resources.NotifyErrorAPIDownload, MessageType.Error);
                    }
                }
                else
                {
                    Alert(Shared.Resources.NotifyErrorAPILock, MessageType.Error);
                }
            }
        }

        /// <summary>
        /// API extend lock (every configured minutes)
        /// </summary>
        public async Task MaintainWork(Work work)
        {
            if (work.State == State.Busy
                && work.IsInSync
                && (!work.LastLock.HasValue || DateTime.Now.Subtract(work.LastLock.Value).TotalMinutes >= Shared.Settings.Default.LockIntervalMinutes))
            {
                var hasLock = await ZaaksysteemClient.HasLockAsync(work.Instruction);
                if (hasLock.GetValueOrDefault())
                {
                    work.IsInSync = await ZaaksysteemClient.ExtendLockAsync(work.Instruction);
                }
                else
                {
                    work.IsInSync = await ZaaksysteemClient.SetLockAsync(work.Instruction);
                }

                if (work.IsInSync)
                {
                    work.LastLock = DateTime.Now;
                }
                else
                {
                    Alert(Shared.Resources.NotifyErrorAPIExtendLock, MessageType.Error);
                }
            }
        }

        /// <summary>
        /// API upload file
        /// API unlock file
        /// DB update database
        /// IO delete from disk
        /// </summary>
        public void RetainWork(Work work)
        {
            work.IsInSync = true;

            StopWork(work);
        }

        /// <summary>
        /// API upload file
        /// API unlock file
        /// DB update database
        /// IO delete from disk
        /// </summary>
        public async void StopWork(Work work)
        {
            Log.Debug($"Stop work for file {work.FileName}");
            work.State = State.Stopping;

            try
            {
                if (!work.IsInSync)
                {
                    Debug.Assert(ConfirmWork != null, "ConfirmWork != null");
                    ConfirmWork(work);
                }
                else
                {
                    var document = _documentService.GetDocument(work.FileName);
                    if (Common.HasHashChanged(work) || work.Hash == null && document.IsModified)
                    {
                        if (await ZaaksysteemClient.UploadAsync(work.Instruction, document))
                        {
                            if (!await ZaaksysteemClient.UnlockAsync(work.Instruction))
                            {
                                Alert(Shared.Resources.NotifyWarningAPIUnlock, MessageType.Warning);
                            }
                            _documentService.DeleteDocument(document.FilePath);

                            work.State = State.Done;
                            Alert(Shared.Resources.NotifyInfoFinishedWork, MessageType.Info);
                        }
                        else
                        {
                            Alert(Shared.Resources.NotifyErrorAPIUpload, MessageType.Error);

                            // retry next time
                            work.State = State.StopFailed;
                            work.IsInSync = false;
                            _fileWatcherService.StartWatcher();
                        }
                    }
                    else
                    {
                        Log.Debug($"No change to file {work.FileName}, unlock and delete document.");
                        if (!await ZaaksysteemClient.UnlockAsync(work.Instruction))
                        {
                            Alert(Shared.Resources.NotifyWarningAPIUnlock, MessageType.Warning);
                        }

                        _documentService.DeleteDocument(document.FilePath);

                        work.State = State.Done;
                        Alert(Shared.Resources.NotifyInfoNoChanges, MessageType.Info);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error($"StopWork function failed with an exception for file {work.FileName}.");
                Log.Debug($"StopWork function failed with an exception for file {work.FileName}.", ex);

                // retry next time
                work.State = State.StopFailed;
                work.IsInSync = false;
                _fileWatcherService.StartWatcher();
            }
        }

        /// <summary>
        /// Removes done work
        /// </summary>
        public void CleanWork()
        {
            if (Shared.Settings.Default.CleanHistory)
            {
                AllWork.Where(w => w.State == State.Done).ToList().ForEach(w => AllWork.Remove(w));
                AllWork.Where(w => w.State == State.Stopping || w.State == State.StopFailed).ToList().ForEach(w => w.State = State.Busy);
            }
        }

        #region event handlers

        private void WorkCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    e.NewItems?.OfType<Work>().ToList().ForEach(w => _database.Add(w));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    e.OldItems?.OfType<Work>().ToList().ForEach(w => _database.Remove(w));
                    break;
            }
        }

        private void WorkItemChanged(object sender, PropertyChangedEventArgs e)
        {
            _database.Update(sender as Work);
        }

        #endregion event handlers

        #region private helpers

        private bool IsWorkInProgress(Instruction instruction)
        {
            // if allready working on the file right now, keep working on current version and show processor
            var workInProgress = AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(instruction.FileId));
            if (workInProgress != null)
            {
                if (workInProgress.State == State.Done)
                {
                    _documentService.DeleteDocument(workInProgress.FileName);
                     AllWork.Remove(workInProgress);
                }
                else if (Common.HasFileLock(workInProgress.FileName))
                {
                    // show document processor
                    _documentService.ActivateDocument(workInProgress.FileName);

                    return true;
                }
            }

            return false;
        }

        #endregion private helpers
    }
}