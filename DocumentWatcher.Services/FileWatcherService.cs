﻿using DocumentWatcher.IO;
using DocumentWatcher.Services.Interfaces;
using DocumentWatcher.Shared.Enums;
using log4net;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Linq;
using System.Timers;
using DocumentWatcher.Models;

namespace DocumentWatcher.Services
{
    /// <summary>
    /// Watch work
    /// </summary>
    public sealed class FileWatcherService : IFileWatcherService, IDisposable
    {
        private static ILog Log => LogManager.GetLogger(typeof(FileWatcherService));
        private readonly Timer _timer;
        private readonly short _maximumWaitingPeriodSecondsForFileLock;

        public FileWatcherService()
        {
            var intervalSeconds = Shared.Settings.Default.FileWatcherIntervalSeconds;
            _maximumWaitingPeriodSecondsForFileLock = Shared.Settings.Default.MaximumWaitingPeriodSecondsForFileLock;            
            Log.Debug($"Starting FileWatcher Service with timer interval of '{intervalSeconds}' seconds and maximum waiting period for file lock of '{_maximumWaitingPeriodSecondsForFileLock}' seconds");
            
            _timer = new Timer(Math.Max((short)1, intervalSeconds) * 1000) { Enabled = true, AutoReset = false };
            _timer.Elapsed += OnTimedEvent;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            var workService = ServiceLocator.Current.GetInstance<IWorkService>();
            try
            {
                foreach (var work in workService.AllWork.Where(w => w.State == State.Busy || w.State == State.StopFailed))
                {
                    if (Common.HasFileLock(work.FileName))
                    {
                        Log.Debug($"File '{work.FileName}' is locked by the document processor");
                        work.FileLockReceived = true;
                        workService.MaintainWork(work);
                    }
                    else if (Common.HasHashChanged(work) || WaitingPeriodForFileLockHasExpired(work, _maximumWaitingPeriodSecondsForFileLock) || work.FileLockReceived)
                    {
                        Log.Debug($"File '{work.FileName}' isn't locked by the document processor anymore, user stopped working on it");
                        workService.StopWork(work);
                    }
                    else
                    {
                        Log.Debug($"File '{work.FileName}' isn't locked but the hash hasn't changed and the maximum waiting period for the file lock hasn't expired yet");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("FileWatcherService timer function failed with an exception.");
                Log.Debug("FileWatcherService timer function failed with an exception.", ex);
                throw;
            }
            finally
            {
                // if files left, start timer again
                if (workService.AllWork.Any(w => w.State != State.Done))
                {
                    _timer.Start();
                }
            }
        }

        /// <summary>
        /// Start polling file locks
        /// </summary>
        public void StartWatcher()
        {
            _timer.Enabled = true;
        }

        /// <summary>
        /// Stop polling file locks
        /// </summary>
        public void StopWatcher()
        {
            _timer.Enabled = false;
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool managed)
        {
            if (managed)
            {
                _timer.Dispose();
            }
        }

        #endregion IDisposable

        private static bool WaitingPeriodForFileLockHasExpired(Work work, short maximumWaitingPeriodSecondsForFileLock)
        {
            return work.WorkStarted.AddSeconds(maximumWaitingPeriodSecondsForFileLock).CompareTo(DateTime.Now) < 0;
        }
        
    }
}