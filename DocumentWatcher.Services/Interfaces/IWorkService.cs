﻿using DocumentWatcher.Models;
using DocumentWatcher.Shared.Enums;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace DocumentWatcher.Services.Interfaces
{
    /// <summary>
    /// Service class for handling work related business logic
    /// </summary>
    public interface IWorkService
    {
        /// <summary>
        /// Message an event
        /// </summary>
        event Action<string, MessageType> Alert;

        /// <summary>
        /// Get all current work
        /// </summary>
        ObservableCollection<Work> AllWork { get; }

        /// <summary>
        /// Clean work history
        /// </summary>
        void CleanWork();

        /// <summary>
        /// Ask for input for a work item
        /// </summary>
        event Action<Work> ConfirmWork;

        /// <summary>
        /// Let user keep working on document
        /// </summary>
        Task MaintainWork(Work work);

        /// <summary>
        /// Keep old version
        /// </summary>
        void RetainWork(Work work);

        /// <summary>
        /// User starts working on document
        /// </summary>
        Task StartWork(Instruction instruction);

        /// <summary>
        /// User stops working on document
        /// </summary>
        void StopWork(Work work);
    }
}