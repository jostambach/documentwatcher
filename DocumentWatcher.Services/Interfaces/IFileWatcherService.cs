﻿namespace DocumentWatcher.Services.Interfaces
{
    /// <summary>
    /// Service class for handling file watcher related business logic
    /// </summary>
    public interface IFileWatcherService
    {
        /// <summary>
        /// Start watching documents
        /// </summary>
        void StartWatcher();

        /// <summary>
        /// Stop/pause watching documents
        /// </summary>
        void StopWatcher();
    }
}