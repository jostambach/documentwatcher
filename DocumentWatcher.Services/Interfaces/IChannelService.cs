﻿namespace DocumentWatcher.Services.Interfaces
{
    /// <summary>
    /// Communicating channel for inter process operations
    /// </summary>
    public interface IChannelService
    {
        /// <summary>
        /// Open communicating channel for inter process operations
        /// </summary>
        void StartChannel();

        /// <summary>
        /// Stop the channel from listening
        /// </summary>
        void StopChannel();

        /// <summary>
        /// Releases ServiceHost
        /// </summary>
        void Dispose();
    }
}