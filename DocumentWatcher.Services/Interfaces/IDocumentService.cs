﻿using DocumentWatcher.Models;

namespace DocumentWatcher.Services.Interfaces
{
    /// <summary>
    /// Service class for handling IO related business logic
    /// </summary>
    public interface IDocumentService
    {
        /// <summary>
        /// Create new document on disk
        /// </summary>
        Document CreateDocument((string fileName, byte[] binary) file);

        /// <summary>
        /// Open document from disk
        /// </summary>
        void OpenDocument(Document document);

        /// <summary>
        /// Get a document from disk
        /// </summary>
        Document GetDocument(string fileName);

        /// <summary>
        /// Open process holding document
        /// </summary>
        void ActivateDocument(string fileName);

        /// <summary>
        /// Delete document from disk
        /// </summary>
        bool DeleteDocument(string filePath);
    }
}