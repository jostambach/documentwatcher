﻿using System.IO;

namespace DocumentWatcher.Models
{
    public class Document
    {
        public string FilePath { get; set; }
        public byte[] Binary { get; set; }

        public bool IsModified
        {
            get
            {
                var isModified = false;

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    var info = new FileInfo(FilePath);
                    isModified = info.LastWriteTime > info.CreationTime;
                }

                return isModified;
            }
        }
    }
}