﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcher.Models")]
[assembly: AssemblyDescription("Model objects")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("DocumentWatcher.Models")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("fb419ef1-d124-45fa-830e-a908445a6589")]

[assembly: AssemblyVersion("1.10.*")]
[assembly: AssemblyFileVersion("1.10")]