﻿using DocumentWatcher.Shared.Enums;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace DocumentWatcher.Models
{
    public class Work : INotifyPropertyChanged
    {
        private DateTime? _lastLock;
        private bool _isInSync;
        private State _state;
        private bool _fileLockReceived;

        public event PropertyChangedEventHandler PropertyChanged;

        #region mappings

        private static IDictionary<State, string> StateMapper => new Dictionary<State, string>
        {
            { State.Busy, Shared.Resources.StateBusy },
            { State.Done, Shared.Resources.StateDone },
            { State.Stopping, Shared.Resources.Stopping },
            { State.StopFailed, Shared.Resources.StopFailed },
        };

        private static IDictionary<string, string> ImageMapper => new Dictionary<string, string>
        {
            { ".doc", "/Images/Icons/word.gif" },
            { ".docx", "/Images/Icons/word.gif" },
            { ".odt", "/Images/Icons/word.gif" },
            { ".xls", "/Images/Icons/excel.gif" },
            { ".xlsx", "/Images/Icons/excel.gif" }
        };

        #endregion mappings

        public string FileName { get; set; }

        public string Image
        {
            get
            {
                var extension = Path.GetExtension(FileName);
                if (extension != null && ImageMapper.ContainsKey(extension))
                {
                    return ImageMapper[extension];   
                }

                return "/Images/Icons/unknown.gif";
            }
        }

        public Instruction Instruction { get; set; }

        public bool IsInSync
        {
            get => _isInSync;
            set
            {
                _isInSync = value;

                NotifyPropertyChanged();
            }
        }

        public DateTime? LastLock
        {
            get => _lastLock;
            set
            {
                _lastLock = value;

                NotifyPropertyChanged();
            }
        }

        public string Location { get; set; }

        public State State
        {
            get => _state;
            set
            {
                _state = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(StateDescription));
            }
        }
        
        public bool FileLockReceived
        {
            get => _fileLockReceived;
            set
            {
                _fileLockReceived = value;

                NotifyPropertyChanged();
            }
        }        
        
        public string StateDescription => StateMapper[State];

        public DateTime WorkStarted { get; set; }
        
        public string Hash { get; set; }

        #region private helpers

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = default)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion private helpers
    }
}