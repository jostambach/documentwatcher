﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcher.Stubs")]
[assembly: AssemblyDescription("Stub web service")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DocumentWatcher.Stubs")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("ad26e6c7-42a9-45ce-a442-6530cc88c2d7")]

[assembly: AssemblyVersion("1.5.*")]
[assembly: AssemblyFileVersion("1.5")]