﻿using log4net;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace DocumentWatcher.Stubs.Controllers
{
    public class FileController : BaseApiController
    {
        private static ILog Log => LogManager.GetLogger(typeof(FileController));
        private static Guid ExcelFileId => new Guid("550e8400-e29b-41d4-a716-446655440001");

        [HttpGet]
        [Route("api/v1/file/{id}/download")]
        public IHttpActionResult Download(Guid id)
        {
            Log.Debug($"Download request received for resouce '{id}'");

            ValidateAuthentication();

            var response = CreateResponse();
            if (id.Equals(ExcelFileId))
            {
                response.Content = new ByteArrayContent(File.ReadAllBytes(Path.Combine(ResourceDirectory, "Aanvraagformulier.xlsx")));
                response.Content.Headers.Add("Content-Disposition", "attachment; filename=Aanvraagformulier.xlsx");
            }
            else
            {
                response.Content = new ByteArrayContent(File.ReadAllBytes(Path.Combine(ResourceDirectory, "Koppeldocument.docx")));
                response.Content.Headers.Add("Content-Disposition", "attachment; filename=Koppeldocument.docx");
            }

            return ResponseMessage(response);
        }

        [HttpPost]
        [Route("api/v1/file/{id}/upload")]
        public async Task<IHttpActionResult> UploadAsync(Guid id)
        {
            var binary = await Request.Content.ReadAsByteArrayAsync();

            Log.Debug($"Upload request received for resouce '{id}' with size {binary.Length}");

            ValidateAuthentication();

            var response = CreateResponse();
            if (!Request.Content.IsMimeMultipartContent())
            {
                response.StatusCode = HttpStatusCode.UnsupportedMediaType;
            }

            if (id.Equals(ExcelFileId))
            {
                // for unit testing
                response.StatusCode = HttpStatusCode.ServiceUnavailable;
            }

            //File.WriteAllBytes($"{ResourceDirectory}{id}", binary);
            //Log.Debug("File is stored");

            return ResponseMessage(response);
        }

        [HttpGet]
        [Route("api/v1/file/{id}/lock")]
        public IHttpActionResult HasLock(Guid id)
        {
            Log.Debug($"Has-lock request received for resouce '{id}'");
            ValidateAuthentication();

            var response = CreateResponse();
            response.StatusCode = HttpStatusCode.NotFound;

            return ResponseMessage(response);
        }

        [HttpPost]
        [Route("api/v1/file/{id}/lock/acquire")]
        public IHttpActionResult SetLock(Guid id)
        {
            Log.Debug($"Set-lock request received for resouce '{id}'");
            ValidateAuthentication();
            return ResponseMessage(CreateResponse());
        }

        [HttpPost]
        [Route("api/v1/file/{id}/lock/extend")]
        public IHttpActionResult ExtendLock(Guid id)
        {
            Log.Debug($"Extend-lock request received for resouce '{id}'");
            ValidateAuthentication();
            return ResponseMessage(CreateResponse());
        }

        [HttpPost]
        [Route("api/v1/file/{id}/lock/release")]
        public IHttpActionResult UnLock(Guid id)
        {
            Log.Debug($"Un-lock request received for resouce '{id}'");
            ValidateAuthentication();
            return ResponseMessage(CreateResponse());
        }

        private HttpResponseMessage CreateResponse()
        {
            var response = Request.CreateResponse();
            response.Headers.AddCookies(new Collection<CookieHeaderValue>
            {
                new CookieHeaderValue(Shared.Settings.Default.SessionCookie, "5e05281ea46e424408a4ec4db1529709f507a25a") { Path = "/", HttpOnly = true }
            });
            return response;
        }
    }
}