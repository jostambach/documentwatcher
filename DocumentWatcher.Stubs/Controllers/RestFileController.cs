﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DocumentWatcher.Stubs.Controllers
{
    public class RestFileController : BaseApiController
    {
        [HttpHead]
        public HttpResponseMessage Head(Guid id)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);

            var fileLocation = Path.Combine(ResourceDirectory, "word.docx");
            var file = File.ReadAllBytes(fileLocation);

            response.Content.Headers.Add("Content-Length", $"{file.Length}");
            response.Content.Headers.Add("Content-Type", MimeMapping.GetMimeMapping(fileLocation));

            return response;
        }

        [HttpOptions]
        public HttpResponseMessage Options(Guid id)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("")
            };
            response.Content.Headers.Add("Allow", "HEAD,OPTIONS,GET,PUT");

            return response;
        }

        [HttpGet]
        public byte[] Get(Guid id)
        {
            return File.ReadAllBytes(Path.Combine(ResourceDirectory, "word.docx"));
        }

        [HttpPut]
        public void Put(Guid id, [FromBody]byte[] content)
        {
            using (var fs = File.Open(Path.Combine(ResourceDirectory, $"{id}.docx"), FileMode.Open, FileAccess.Write))
            {
                using (var writer = new BinaryWriter(File.Open(Path.Combine(ResourceDirectory, $"{id}.docx"), FileMode.Create)))
                {
                    writer.Write(content);
                }
            }
        }
    }
}