﻿using DocumentWatcher.Models;
using DocumentWatcher.ViewModels;
using System.Windows;

namespace DocumentWatcher.Views
{
    /// <summary>
    /// Interaction logic for ConfirmWindow.xaml
    /// </summary>
    public partial class ConfirmWindow
    {
        public ConfirmWindow(Work oldWork)
        {
            InitializeComponent();

            DataContext = new ConfirmWindowViewModel(oldWork);
        }

        private void ButtonYes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonNo_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}