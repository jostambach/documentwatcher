﻿using DocumentWatcher.Models;
using DocumentWatcher.ViewModels;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;

namespace DocumentWatcher.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public Action<string> WorkAdded { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
            ((MainWindowViewModel)DataContext).Work.CollectionChanged += Work_CollectionChanged;
        }

        private void Work_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                e.NewItems?.OfType<Work>().ToList().ForEach(w => WorkAdded(w.FileName));
            }
        }
    }
}