﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("Zaaksysteem Document Watcher")]
[assembly: AssemblyDescription("User interfaces")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("Document Watcher")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]

[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("nl-NL")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, //where theme specific resource dictionaries are located
                                     //(used if a resource is not found in the page,
                                     // or application resource dictionaries)
    ResourceDictionaryLocation.SourceAssembly //where the generic resource dictionary is located
                                              //(used if a resource is not found in the page,
                                              // app, or any theme specific resource dictionaries)
)]

// Version information for an assembly consists of the following four values:
//      Major Version
//      Minor Version
//      Build Number
//      Revision
[assembly: AssemblyVersion("1.10.*")]
[assembly: AssemblyFileVersion("1.10")]