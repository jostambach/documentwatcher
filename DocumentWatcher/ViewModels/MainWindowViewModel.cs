﻿using DocumentWatcher.Services.Interfaces;
using System.ComponentModel;
using System.Windows.Data;
using Microsoft.Practices.ServiceLocation;

namespace DocumentWatcher.ViewModels
{
    public class MainWindowViewModel
    {
        public ICollectionView Work { get; }

        public MainWindowViewModel()
        {
            var workService = ServiceLocator.Current.GetInstance<IWorkService>();

            Work = CollectionViewSource.GetDefaultView(workService.AllWork);
        }
    }
}