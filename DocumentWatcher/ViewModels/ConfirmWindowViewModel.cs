﻿using DocumentWatcher.Models;
using System.Globalization;

namespace DocumentWatcher.ViewModels
{
    public class ConfirmWindowViewModel
    {
        public string FileName { get; }
        public string FileNameLabel => string.Format(Shared.Resources.ConfirmWindowQuestion, FileName, CultureInfo.CurrentCulture);

        public ConfirmWindowViewModel(Work oldWork)
        {
            FileName = oldWork?.FileName;
        }
    }
}