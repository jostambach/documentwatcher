﻿using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;

namespace DocumentWatcher.IO.Data
{
    public class DataFile : IDatabase
    {
        private static ILog Log => LogManager.GetLogger(typeof(DataFile));
        private static string DatabasePath => Path.Combine(Common.HomeDirectory, "work.json");

        private readonly Collection<Work> _items;

        public DataFile()
        {
            _items = new Collection<Work>();

            if (File.Exists(DatabasePath))
            {
                try
                {
                    Log.Debug($"Opening data file {DatabasePath}");
                    var data = File.ReadAllText(DatabasePath);

                    Log.Debug("Deserializing data file");
                    _items = JsonConvert.DeserializeObject<Collection<Work>>(data);
                }
                catch (Exception ex)
                {
                    Log.Fatal("Error opening data file", ex);
                }
            }
        }

        public ICollection<Work> All => _items;

        public bool Add(Work work)
        {
            Directory.CreateDirectory(Common.HomeDirectory);

            Monitor.Enter(_items);
            try
            {
                if (!_items.Any(w => w.Instruction.FileId.Equals(work.Instruction.FileId)))
                {
                    Log.Debug("Adding item to data file");

                    _items.Add(work);
                    File.WriteAllText(DatabasePath, JsonConvert.SerializeObject(_items));

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal("Error persisting data file", ex);
            }
            finally
            {
                Monitor.Exit(_items);
            }

            Log.Warn("Item allready exists");
            return false;
        }

        public bool Update(Work work)
        {
            Directory.CreateDirectory(Common.HomeDirectory);

            Log.Debug($"Updating item (with state {work.State}) in data file");

            Monitor.Enter(_items);
            try
            {
                var match = _items.SingleOrDefault(w => w.Instruction.FileId.Equals(work.Instruction.FileId));
                if (match != null)
                {
                    _items.Remove(match);
                    _items.Add(work);
                    File.WriteAllText(DatabasePath, JsonConvert.SerializeObject(_items));

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal("Error persisting data file", ex);
            }
            finally
            {
                Monitor.Exit(_items);
            }

            Log.Warn("Item not found");
            return false;
        }

        public bool Remove(Work work)
        {
            Log.Debug("Removing item from data file");

            Monitor.Enter(_items);
            try
            {
                var match = _items.SingleOrDefault(w => w.Instruction.FileId.Equals(work.Instruction.FileId));
                if (match != null)
                {
                    _items.Remove(match);
                    File.WriteAllText(DatabasePath, JsonConvert.SerializeObject(_items));

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal("Error persisting data file", ex);
            }
            finally
            {
                Monitor.Exit(_items);
            }

            Log.Warn("Item not found");
            return false;
        }
    }
}