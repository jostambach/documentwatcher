﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcher.IO")]
[assembly: AssemblyDescription("External communication")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("DocumentWatcher.IO")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("7c41c519-0f10-4aad-b8c3-026940d75ad1")]

[assembly: AssemblyVersion("1.10.*")]
[assembly: AssemblyFileVersion("1.10")]