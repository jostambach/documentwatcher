﻿using DocumentWatcher.Models;
using log4net;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DocumentWatcher.IO.API
{
    public static class ZaaksysteemClient
    {
        private static ILog Log => LogManager.GetLogger(typeof(ZaaksysteemClient));

        public static async Task<bool?> HasLockAsync(Instruction instruction)
        {
            bool? hasLock = default;

            Log.Debug("Getting lock status of document");

            using (var client = CreateClient(instruction, out var cookieJar))
            {
                try
                {
                    using (var response = await client.GetAsync(instruction.HasLock))
                    {
                        PersistSession(instruction, cookieJar);

                        switch (response.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                hasLock = true;
                                Log.Debug("Document is currently locked");
                                break;
                            case HttpStatusCode.NotFound:
                                hasLock = false;
                                Log.Debug("Document has no lock");
                                break;
                            default:
                                LogError(response);
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"Error getting lock status of document {instruction.BaseUri}.");
                    Log.Debug($"Error getting lock status of document {instruction.BaseUri}.", ex);
                }

                return hasLock;
            }
        }

        public static async Task<bool> SetLockAsync(Instruction instruction)
        {
            Log.Debug("Setting lock on document");

            using (var client = CreateClient(instruction, out var cookieJar))
            try
            {
                using (var response = await client.PostAsync(instruction.SetLock, null))
                {
                    PersistSession(instruction, cookieJar);

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError(response);
                    }

                    return response.IsSuccessStatusCode;
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error setting lock on document {instruction.BaseUri}.");
                Log.Debug($"Error setting lock on document {instruction.BaseUri}.", ex);
                return false;
            }
        }

        public static async Task<bool> ExtendLockAsync(Instruction instruction)
        {
            Log.Debug("Extending lock on document");

            using (var client = CreateClient(instruction, out var cookieJar))
            try
            {
                using (var response = await client.PostAsync(instruction.ExtendLock, null))
                {
                    PersistSession(instruction, cookieJar);

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError(response);
                    }

                    return response.IsSuccessStatusCode;
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error extending lock on document {instruction.BaseUri}.");
                Log.Debug($"Error extending lock on document {instruction.BaseUri}.", ex);
                return false;
            }
        }

        public static async Task<bool> UnlockAsync(Instruction instruction)
        {
            Log.Debug("Unlocking document");

            using (var client = CreateClient(instruction, out var cookieJar))
            try
            {
                using (var response = await client.PostAsync(instruction.Unlock, null))
                {
                    PersistSession(instruction, cookieJar);

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError(response);
                    }

                    return response.IsSuccessStatusCode;
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error unlocking document {instruction.BaseUri}.");
                Log.Debug($"Error unlocking document {instruction.BaseUri}.", ex);
                return false;
            }
        }

        public static async Task<(string fileName, byte[] binary)> DownloadAsync(Instruction instruction)
        {
            Log.Debug("Downloading document");

            using (var client = CreateClient(instruction, out var cookieJar))
            try
            {
                using (var response = await client.GetAsync(instruction.Download))
                {
                    PersistSession(instruction, cookieJar);

                    (string fileName, byte[] binary) file = default;
                    if (response.IsSuccessStatusCode && (response.Content?.Headers?.ContentDisposition?.DispositionType ?? "").Equals("attachment"))
                    {
                        /* filename can be provided in 2 formats
                            * 
                            * format 1:
                            *   "Koppeldocument.docx"
                            * format 2:
                            *   "objectaanvraag.docx; filename*=utf-8''objectaanvraag.docx"
                            */
                        file.fileName = response.Content?.Headers?.ContentDisposition?.FileName;
                        file.fileName = file.fileName?.Substring(0, file.fileName.IndexOf(';') < 0 ? file.fileName.Length : file.fileName.IndexOf(';')).CleanFileName();
                        Log.Debug($"Document '{file.fileName}' succesfully downloaded");

                        file.binary = await response.Content?.ReadAsByteArrayAsync();
                    }
                    else
                    {
                        LogError(response);
                    }

                    return file;
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error downloading document {instruction.BaseUri}.");
                Log.Debug($"Error downloading document {instruction.BaseUri}.", ex);
                return default;
            }
        }

        public static async Task<bool> UploadAsync(Instruction instruction, Document document)
        {
            Log.Debug("Uploading document");

            using (var client = CreateClient(instruction, out var cookieJar))
            try
            {
                using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new ByteArrayContent(document.Binary), "file", Path.GetFileName(document.FilePath));

                    using (var response = await client.PostAsync(instruction.Upload, content))
                    {
                        PersistSession(instruction, cookieJar);

                        if (!response.IsSuccessStatusCode)
                        {
                            LogError(response);
                        }

                        return response.IsSuccessStatusCode;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error uploading document {instruction.BaseUri}.");
                Log.Debug($"Error uploading document {instruction.BaseUri}.", ex);
                return false;
            }
        }

        #region private helpers

        private static HttpClient CreateClient(Instruction instruction, out CookieContainer cookieJar)
        {
            cookieJar = new CookieContainer();
            if (!string.IsNullOrWhiteSpace(instruction.SessionId))
            {
                cookieJar.Add(instruction.BaseUri, new Cookie(Shared.Settings.Default.SessionCookie, instruction.SessionId, "/") { HttpOnly = true, Secure = true });
            }

            var client = new HttpClient(new HttpClientHandler { CookieContainer = cookieJar }) { BaseAddress = instruction.BaseUri, Timeout = TimeSpan.FromSeconds(100) };
            client.DefaultRequestHeaders.TryAddWithoutValidation(Shared.Settings.Default.AuthenticationHeader, instruction.AuthenticationToken);

            return client;
        }

        private static void PersistSession(Instruction instruction, CookieContainer cookieJar)
        {
            var sessionCookie = cookieJar.GetCookies(instruction.BaseUri).Cast<Cookie>().SingleOrDefault(c => c.Name == Shared.Settings.Default.SessionCookie);
            if (sessionCookie != null
                && sessionCookie.HttpOnly
#if !DEBUG
                && sessionCookie.Secure
#endif
                && !sessionCookie.Expired
                && !string.IsNullOrWhiteSpace(sessionCookie.Value))
            {
                instruction.SessionId = sessionCookie.Value;
            }
            else
            {
                instruction.SessionId = null;
            }
        }

        private static async void LogError(HttpResponseMessage response, [CallerMemberName] string operation = "Operation")
        {
            if (response?.Content != null)
            {
                Log.Error($"{operation} failed.{Environment.NewLine}{Environment.NewLine}<REQUEST>{Environment.NewLine}{response.RequestMessage.RequestUri} {response.RequestMessage.Method}{Environment.NewLine}</REQUEST>");
                var responseMessage = await response.Content.ReadAsStringAsync();
                Log.Debug($"{operation} failed.{Environment.NewLine}{Environment.NewLine}<REQUEST>{Environment.NewLine}{response.RequestMessage}{Environment.NewLine}</REQUEST>{Environment.NewLine}{Environment.NewLine}<RESPONSE>{Environment.NewLine}{response},{Environment.NewLine}Body: {responseMessage}{Environment.NewLine}</RESPONSE>");
            } 
            else
            {
                Log.Error($"{operation} failed.{Environment.NewLine}{Environment.NewLine}<REQUEST>{Environment.NewLine}");
            }
        }

        #endregion private helpers
    }
}