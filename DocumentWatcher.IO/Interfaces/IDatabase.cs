﻿using DocumentWatcher.Models;
using System.Collections.Generic;

namespace DocumentWatcher.IO.Interfaces
{
    /// <summary>
    /// Internal database for persistance of state
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// Retreive all items from database
        /// </summary>
        ICollection<Work> All { get; }

        /// <summary>
        /// Add an item to the database
        /// </summary>
        bool Add(Work work);

        /// <summary>
        /// Update an item in the database
        /// </summary>
        bool Update(Work work);

        /// <summary>
        /// Remove an item in the database
        /// </summary>
        bool Remove(Work work);
    }
}