﻿using DocumentWatcher.Models;
using System.ServiceModel;

namespace DocumentWatcher.IO.Interfaces
{
    /// <summary>
    /// Host for transferring documents
    /// </summary>
    [ServiceContract]
    public interface IDocumentHost
    {
        /// <summary>
        /// Add a new request to the channel
        /// </summary>
        [OperationContract]
        void AddInstruction(Instruction instruction);
    }
}