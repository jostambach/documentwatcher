﻿using System;
using System.IO;
using System.Security.Cryptography;
using DocumentWatcher.Models;

namespace DocumentWatcher.IO
{
    public static class Common
    {
        /// <summary>
        /// Get base directory
        /// </summary>
        public static string HomeDirectory => Path.Combine(
            CustomTempDirectorySet ?
                Shared.Settings.Default.TempDirectory.Replace(@"\\", @"\") :
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "Zaaksysteem", "DocumentWatcher");

        /// <summary>
        /// Get working directory
        /// </summary>
        public static string WorkingDirectory => Path.Combine(HomeDirectory, "work");

        public static bool CustomTempDirectorySet => !string.IsNullOrWhiteSpace(Shared.Settings.Default.TempDirectory);

        /// <summary>
        /// Determine if file has exclusive lock, based on opening with no sharing
        /// </summary>
        public static bool HasFileLock(string fileName)
        {
            try
            {
                var filePath = Path.Combine(WorkingDirectory, fileName);
                if (File.Exists(filePath))
                {
                    using (new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        return false;
                    }
                }
            }
            catch (IOException)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will return true in case the hash of the file thw Work object references has changed.
        /// If no original hash is available or the current hash could not be calculated it will return false
        /// </summary>   
        public static bool HasHashChanged(Work work)
        {
            var currentHash = CalculateHash(work.FileName);
            return work.Hash != null && currentHash != null && !currentHash.Equals(work.Hash);
        }         
        
        /// <summary>
        /// Will calculate the MD5 hash for the byte array specified
        /// </summary>        
        public static string CalculateHash(byte[] fileAsBytes)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(fileAsBytes)).Replace("-", "").ToUpperInvariant();              
            }
        }
        
        
        /// <summary>
        /// Will calculate the MD5 hash for the file specified, returns null in case the file was not found/locked or an other IO error occured
        /// </summary>        
        private static string CalculateHash(string fileName)
        {
            try
            {
                return CalculateHash(File.ReadAllBytes(Path.Combine(WorkingDirectory, fileName)));
            }
            catch (IOException)
            {
                return null;
            }
        }        
    }
}