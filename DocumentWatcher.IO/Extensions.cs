﻿using System.IO;
using System.Linq;

namespace DocumentWatcher.IO
{
    public static class Extensions
    {
        public static string CleanFileName(this string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
    }
}