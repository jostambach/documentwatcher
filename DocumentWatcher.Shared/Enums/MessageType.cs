﻿namespace DocumentWatcher.Shared.Enums
{
    public enum MessageType
    {
        None,
        Info,
        Warning,
        Error
    }
}