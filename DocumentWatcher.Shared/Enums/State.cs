﻿namespace DocumentWatcher.Shared.Enums
{
    public enum State
    {
        Busy,
        Done,
        Stopping,
        StopFailed
    }
}