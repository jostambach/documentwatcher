﻿using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DocumentWatcher.Tests.Mocks
{
    internal class DataFileMock : IDatabase
    {
        private Collection<Work> _items = new Collection<Work>();

        public ICollection<Work> All => _items;

        public bool Add(Work work)
        {
            _items.Add(work);

            return true;
        }

        public bool Update(Work work)
        {
            Remove(work);
            Add(work);

            return true;
        }

        public bool Remove(Work work)
        {
            _items.Remove(work);

            return true;
        }
    }
}