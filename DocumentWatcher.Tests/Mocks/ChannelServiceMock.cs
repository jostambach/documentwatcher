﻿using DocumentWatcher.Services.Interfaces;

namespace DocumentWatcher.Tests.Mocks
{
    internal class ChannelServiceMock : IChannelService
    {
        public void StartChannel()
        { }

        public void StopChannel()
        { }

        public void Dispose()
        { }
    }
}
