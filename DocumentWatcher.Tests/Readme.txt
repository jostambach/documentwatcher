﻿The Unit Tests in this project requires the website DocumentWatcher.Stubs to be hosted locally.

The Debug configuration requires the HTTP protocol, the Release configuration requires HTTPS.

When using Local IIS, start Visual Studio with elevated privileges.