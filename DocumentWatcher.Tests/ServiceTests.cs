﻿using DocumentWatcher.IO;
using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Models;
using DocumentWatcher.Services;
using DocumentWatcher.Services.Interfaces;
using DocumentWatcher.Shared.Enums;
using DocumentWatcher.Tests.Mocks;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DocumentWatcher.Tests
{
    [TestClass]
    public class ServiceTests
    {
        private static Guid WordFileId => new Guid("EC3D8455-D61F-4DB7-A331-CB4B00983A9E");
        private static Guid ExcelFileId => new Guid("550e8400-e29b-41d4-a716-446655440001");

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // register mocks
            SimpleIoc.Default.Register<IChannelService, ChannelServiceMock>();
            SimpleIoc.Default.Register<IDatabase, DataFileMock>();
            SimpleIoc.Default.Register<IDocumentService, DocumentServiceMock>();
            SimpleIoc.Default.Register<IFileWatcherService, FileWatcherServiceMock>();

            // register actual business logic
            SimpleIoc.Default.Register<IWorkService, WorkService>();
        }

        #endregion Initialize

        [TestMethod]
        public void WorkService_Test()
        {
            Assert_WorkService_StartWork();
            Assert_WorkService_MaintainWork();
            Assert_WorkService_RetainWork();
            Assert_WorkService_StopWork();
        }

        private void Assert_WorkService_StartWork()
        {
            var database = ServiceLocator.Current.GetInstance<IDatabase>();
            var data = database.All;

            var workService = ServiceLocator.Current.GetInstance<IWorkService>();
            Assert.AreEqual(0, workService.AllWork.Count);
            workService.Alert += (message, type) =>
            {
                Assert.IsNotNull(message);
                if (message == Shared.Resources.NotifyErrorAPIUpload)
                {
                    Assert.AreEqual(MessageType.Error, type);
                }
                else
                {
                    Assert.AreEqual(MessageType.Info, type);
                }
            };

            #region word file

            var wordInstruction = new Instruction
            {
                BaseAddress = $"localhost/DocumentWatcher.Stubs/api/v1/file/{WordFileId}",
                AuthenticationToken = "419240cd3d90c5eb",
                Version = 2,
                Download = "download",
                Upload = "upload",
                HasLock = "lock",
                SetLock = "lock/acquire",
                ExtendLock = "lock/extend",
                Unlock = "lock/release"
            };
            wordInstruction.Init();

            workService.StartWork(wordInstruction).Wait();
            Assert.AreEqual(1, workService.AllWork.Count);
            Assert.AreEqual(1, data.Count);

            var word = workService.AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(WordFileId));
            Assert.AreEqual("http://localhost/", word?.Location);
            Assert.IsTrue((word?.IsInSync).GetValueOrDefault());
            Assert.AreEqual("Koppeldocument.docx", word?.FileName);
            Assert.AreEqual("/Images/Icons/word.gif", word?.Image);
            Assert.AreEqual(Shared.Resources.StateBusy, word?.StateDescription);

            workService.CleanWork();
            Assert.AreEqual(1, database.All.Count);

            #endregion word file

            #region excel file

            var excelInstruction = new Instruction
            {
                Download = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/download",
                Upload = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/upload",
                HasLock = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/lock",
                SetLock = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/lock/acquire",
                ExtendLock = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/lock/extend",
                Unlock = $"/DocumentWatcher.Stubs/api/v1/file/{ExcelFileId}/lock/release"
            };
            excelInstruction.Init();

            Assert.IsFalse(excelInstruction.IsValid());
            excelInstruction.BaseAddress = "localhost";
            excelInstruction.AuthenticationToken = "92a245d5a7e75724";
            Assert.IsTrue(excelInstruction.IsValid());
            workService.StartWork(excelInstruction).Wait();

            var excel = workService.AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(ExcelFileId));
            Assert.AreEqual("Aanvraagformulier.xlsx", excel?.FileName);
            Assert.AreEqual("/Images/Icons/excel.gif", excel?.Image);

            Assert.AreEqual(2, workService.AllWork.Count);
            Assert.AreEqual(2, database.All.Count);

            #endregion excel file
        }

        private void Assert_WorkService_MaintainWork()
        {
            var workService = ServiceLocator.Current.GetInstance<IWorkService>();
            var word = workService.AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(WordFileId));
            Assert.IsNotNull(word);

            word.LastLock = DateTime.Now.AddHours(-5);
            workService.MaintainWork(word).Wait();
            Assert.IsTrue(word.IsInSync);
            Assert.AreEqual(0, DateTime.Now.Subtract(word.LastLock.Value).Minutes);
        }

        private void Assert_WorkService_RetainWork()
        {
            var workService = ServiceLocator.Current.GetInstance<IWorkService>();
            workService.ConfirmWork += oldWork =>
            {
                Assert.AreEqual(State.Done, oldWork.State);
                Assert.IsFalse(oldWork.IsInSync);

                workService.RetainWork(oldWork);
                Assert.IsTrue(oldWork.IsInSync);
            };
            var excel = workService.AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(ExcelFileId));
            Assert.IsNotNull(excel);

            excel.IsInSync = false;
            workService.StopWork(excel);
        }

        private void Assert_WorkService_StopWork()
        {
            var workService = ServiceLocator.Current.GetInstance<IWorkService>();
            var database = ServiceLocator.Current.GetInstance<IDatabase>();

            var word = workService.AllWork.SingleOrDefault(w => w.Instruction.FileId.Equals(WordFileId));
            Assert.IsNotNull(word);
            Assert.AreEqual(State.Busy, word.State);

            workService.StopWork(word);
            Assert.AreEqual(State.Done, word.State);
            Assert.AreEqual(Shared.Resources.StateDone, word.StateDescription);
            Assert.IsFalse(Common.HasFileLock(word.FileName));

            Assert.AreEqual(2, database.All.Count);
            workService.CleanWork();
            Assert.AreEqual(0, database.All.Count);
        }
    }
}