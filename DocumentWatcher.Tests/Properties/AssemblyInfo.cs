using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcherTests")]
[assembly: AssemblyDescription("Document mediator between client and server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("DocumentWatcherTests")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("c63e0051-1f1e-4b6b-a55d-ec299e22ffc0")]

[assembly: AssemblyVersion("1.10.*")]
[assembly: AssemblyFileVersion("1.10")]